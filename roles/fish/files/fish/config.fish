set fish_greeting ""

# anaconda
if test -d /opt/anaconda/current/bin
	set -x PATH /opt/anaconda/current/bin $PATH
end
