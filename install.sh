#!/bin/sh

###################
# Install ansible #
if ! grep -q "ansible/ansible" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
    echo "Adding Ansible PPA"
    sudo apt-add-repository ppa:ansible/ansible -y
fi

echo "Installing Ansible..."
sudo apt update
sudo apt install software-properties-common ansible git -y

echo "Cloning Repository"
cd ~/.config
git clone https://markusschneider@bitbucket.org/markusschneider/ansible-ubuntu.git
cd ansible-ubuntu

echo "Execute the following command to run playbook:"
echo "ansible-playbook ansible-desktop.yml --ask-become-pass"
