## Installation
First, you need to install Git and Ansible :
```
curl -fsSL https://bitbucket.org/markusschneider/ansible-ubuntu/raw/HEAD/install.sh | sh
```

Run `ansible-playbook ansible-desktop.yml --ask-become-pass` and enter your sudo password to run the playbook
